package jrgd.particlelib;

import jrgd.particlelib.Types.LPEGroup;
import jrgd.particlelib.Types.LoopedParticleEffect;
import jrgd.particlelib.Types.SerializedLPE;
import jrgd.particlelib.Types.SerializedLPEGroup;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

import static jrgd.particlelib.LoopedParticleRegistry.AddEffect;
import static jrgd.particlelib.LoopedParticleRegistry.AddGroup;

public class io {
    static File directoryPath = new File("mods/jrgd_particlelib");

    static boolean serializeEffectOut(SerializedLPE effect) {
        boolean check = true;
        try {
            String fileName= effect.getName();
            FileOutputStream fos = new FileOutputStream("mods/jrgd_particlelib/" + fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(effect);
            oos.close();
        } catch(Exception e) {
            check = false;
        }
        return check;
    }

    static boolean serializeGroupOut(SerializedLPEGroup group) {
        boolean check = true;
        try {
            String fileName= group.getName();
            FileOutputStream fos = new FileOutputStream("mods/jrgd_particlelib/!" + fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(group);
            oos.close();
        } catch(Exception e) {
            check = false;
        }
        return check;
    }

    static LoopedParticleEffect serializeEffectIn(String effect) throws IOException, ClassNotFoundException {
        FileInputStream fin = new FileInputStream("mods/jrgd_particlelib/" + effect);
        ObjectInputStream ois = new ObjectInputStream(fin);
        SerializedLPE particle = (SerializedLPE) ois.readObject();
        ois.close();
        return particle.construct();
    }

    static LPEGroup serializeGroupIn(String effect) throws IOException, ClassNotFoundException {
        FileInputStream fin = new FileInputStream("mods/jrgd_particlelib/" + effect);
        ObjectInputStream ois = new ObjectInputStream(fin);
        SerializedLPEGroup group = (SerializedLPEGroup) ois.readObject();
        ois.close();
        return group.construct();
    }

    static ArrayList<String> getDataFiles() {
        String[] files = directoryPath.list();
        return new ArrayList(Arrays.asList(files));
    }

    public static boolean LoadFile(String file, boolean loadGroups) {
        boolean returnValue = true;
        LPEGroup g = null;
        LoopedParticleEffect f = null;

        if (loadGroups) {
            if (file.startsWith("!")) {
                try {
                    g = serializeGroupIn(file);

                } catch (IOException | ClassNotFoundException e) {
                    System.out.println("File 'mods/jrgd_particlelib/" + file + "' not recognized as valid LPEGroup.");
                    returnValue = false;
                }
                if (g != null) AddGroup(g);
                return returnValue;
            }
            return false;
        }
        else {
            if (!file.startsWith("!")) {
                try {
                    f = serializeEffectIn(file);

                } catch (IOException | ClassNotFoundException e) {
                    System.out.println("File 'mods/jrgd_particlelib/" + file + "' not recognized as valid LPE.");
                    returnValue = false;
                }
                if (f != null) AddEffect(f);
                return returnValue;
            }
            return false;
        }
    }

    public static boolean DeleteFile(String file) {
        File target = new File("mods/jrgd_particlelib/" + file);
        if (target.delete()) return true;
        else return false;
    }
}
