package jrgd.particlelib.Types;

public class Vector3d {
    public double x;
    public double y;
    public double z;

    public Vector3d(double X, double Y, double Z) {
        this.x = X;
        this.y = Y;
        this.z = Z;
    }
    public boolean isSame(Vector3d a) {
        if (this.x == a.x &&
        this.y == a.y &&
        this.z == a.z) {
            return true;
        }
        else
            return false;
    }

    public Vector3d add(Vector3d input) {
        this.x += input.x;
        this.y += input.y;
        this.z += input.z;
        return this;
    }

    public Vector3d subtract(Vector3d input) {
        this.x -= input.x;
        this.y -= input.y;
        this.z -= input.z;
        return this;
    }

    public Vector3d multiply(Vector3d input) {
        this.x *= input.x;
        this.y *= input.y;
        this.z *= input.z;
        return this;
    }

    public Vector3d multiply(Double input) {
        this.x *= input;
        this.y *= input;
        this.z *= input;
        return this;
    }

    public Vector3d divide(Vector3d input) {
        this.x /= input.x;
        this.y /= input.y;
        this.z /= input.z;
        return this;
    }

    public Vector3d divide(Double input) {
        this.x /= input;
        this.y /= input;
        this.z /= input;
        return this;
    }

    public Double magnitude() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public Vector3d normalize() {
        this.x *= 1/this.magnitude();
        this.y *= 1/this.magnitude();
        this.z *= 1/this.magnitude();
        return this;
    }

    public double distance(Vector3d input) {
        Vector3d distance = new Vector3d(0,0,0);

        distance.x = (input.x - this.x) * (input.x - this.x);
        distance.y = (input.y - this.y) * (input.y - this.y);
        distance.z = (input.z - this.z) * (input.z - this.z);

        return Math.sqrt(distance.x + distance.y + distance.z);
    }
}
