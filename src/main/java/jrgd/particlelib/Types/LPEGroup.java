package jrgd.particlelib.Types;

import java.util.ArrayList;
import java.util.Arrays;

public class LPEGroup {
    private ArrayList<LoopedParticleEffect> group;
    private String name;

    public LPEGroup(String Name, LoopedParticleEffect Effect) {
        this.name = Name.replaceAll("[^a-zA-Z0-9-_]","");
        this.group = (ArrayList<LoopedParticleEffect>) Arrays.asList(Effect);
    }

    public LPEGroup(String Name) {
        this.name = Name.replaceAll("[^a-zA-Z0-9-_]","");
        this.group = new ArrayList<>();
    }

    public LPEGroup(String Name, ArrayList<LoopedParticleEffect> Effects) {
        this.name = Name.replaceAll("[^a-zA-Z0-9-_]","");
        this.group = Effects;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<LoopedParticleEffect> getEffects() {
        return this.group;
    }

    public ArrayList<LoopedParticleEffect> addEffect(LoopedParticleEffect Effect) {
        this.group.add(Effect);
        return group;
    }

    public ArrayList<LoopedParticleEffect> addEffects(ArrayList<LoopedParticleEffect> Effects) {
        this.group.addAll(Effects);
        return this.group;
    }
}
