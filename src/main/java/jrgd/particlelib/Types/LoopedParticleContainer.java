package jrgd.particlelib.Types;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;

public class LoopedParticleContainer {
    LoopedParticleEffect particle;
    PlayerEntity player;
    ServerWorld world;
    Vector3d worldCoordinates;
    boolean entity;

    public LoopedParticleContainer(LoopedParticleEffect Particle, PlayerEntity Player) {
        this.particle = Particle;
        this.player = Player;
        this.entity = true;
    }

    public LoopedParticleContainer(LoopedParticleEffect Particle, ServerWorld DisplayWorld, Vector3d Coordinates) {
        this.particle = Particle;
        this.world = DisplayWorld;
        this.entity = false;
        this.worldCoordinates = Coordinates;
    }

    public LoopedParticleContainer(LoopedParticleEffect Particle, ServerWorld DisplayWorld, double X, double Y, double Z) {
        this.particle = Particle;
        this.world = DisplayWorld;
        this.entity = false;
        this.worldCoordinates = new Vector3d(X,Y,Z);
    }

    public LoopedParticleEffect getLoopedParticle() {
        return particle;
    }

    public PlayerEntity getPlayer() {
        return player;
    }

    public ServerWorld getWorld() {
        return world;
    }

    public Vector3d getWorldCoordinates() {return worldCoordinates;}

    public boolean isEntityReference() {
        return entity;
    }
}
