package jrgd.particlelib.Types;

import jrgd.particlelib.Types.ParticlePrefs.BlockParticlePrefs;
import jrgd.particlelib.Types.ParticlePrefs.DustParticlePrefs;
import jrgd.particlelib.Types.ParticlePrefs.ItemStackParticlePrefs;
import net.minecraft.particle.*;

import java.io.Serializable;
import java.util.ArrayList;

public class SerializedLPE implements Serializable {

    //   primary fields
    private String name;

    private int delay;
    private int offset;
    private boolean reverse;
    private int index;
    private int renderCount;
    private int renderSteps;

    //   ParticlePrefab fields

    private String globalParticle = null;
    private String globalParticleData = null;

    // particle type + parameters
    private String[] particle;

    // positions
    private double[] posx;
    private double[] posy;
    private double[] posz;

    // deltas
    private double[] deltax;
    private double[] deltay;
    private double[] deltaz;

    private double[] speed; // speed of particle effect according to minecraft
    private int[] count;
    private boolean[] keyframe;

    // construct serializable from LPE
    public SerializedLPE(LoopedParticleEffect lpe) {
        this.name = lpe.getName();
        this.delay = lpe.getDelay();
        this.offset = lpe.getOffset();
        this.reverse = lpe.isReversed();
        this.index = lpe.getIndex();
        this.renderCount = lpe.getRenderCount();
        this.renderSteps = lpe.getRenderSteps();

        // get size of data to store to arrays
        int ref = lpe.getData().size();

        // set size of data stores to LPE data size
        this.particle = new String[ref];
        this.posx = new double[ref];
        this.posy = new double[ref];
        this.posz = new double[ref];
        this.deltax = new double[ref];
        this.deltay = new double[ref];
        this.deltaz = new double[ref];
        this.speed = new double[ref];
        this.count = new int[ref];
        this.keyframe = new boolean[ref];

        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> parameters = new ArrayList<>();

        // insert data from LPE data into stores
        int frame = 0;
        for (ParticlePrefab particle : lpe.getData()) {

            if (particle.getParticle().asString().contains("minecraft:item")) {
                String data = particle.getParticle().asString();
                data = data.substring(data.indexOf("ISPE_DEF"));
                data = data.substring(data.indexOf('"') + 1);
                data = "item " + data.substring(data.indexOf(':') + 1,data.indexOf('"'));
                this.particle[frame] = data;
            }
            else {
                this.particle[frame] = particle.getParticle().asString().replace("minecraft:","");
            }

            // initial checks for how to compress data
            if (this.particle[frame].indexOf(' ') != -1) {
                names.add(this.particle[frame].substring(0, this.particle[frame].indexOf(' ')));
                parameters.add(this.particle[frame].substring(this.particle[frame].indexOf(' ') + 1));
            }
            else {
                names.add(this.particle[frame]);
            }

            this.posx[frame] = particle.getPosition().x;
            this.posy[frame] = particle.getPosition().y;
            this.posz[frame] = particle.getPosition().z;
            this.deltax[frame] = particle.getOption().x;
            this.deltay[frame] = particle.getOption().y;
            this.deltaz[frame] = particle.getOption().z;
            this.speed[frame] = particle.getSpeed();
            this.count[frame] = particle.getCount();
            this.keyframe[frame] = particle.isKeyframe();
            frame++;
        }

        // compression after initial packing
        String refName = names.get(0);
        boolean refNameCheck = true;
        boolean refDataCheck = names.size() == parameters.size();
        String refData = null;
        if (refDataCheck) refData = parameters.get(0);

        for (String name : names) {
            if (!refName.equals(name)) {
                refNameCheck = false;
                refDataCheck = false;
                break;
            }
        }

        if (refDataCheck) {
            for (String parameter : parameters) {
                if (!refData.equals(parameter)) {
                    refDataCheck = false;
                    break;
                }
            }
        }

        if (refNameCheck) {
            int index = 0;
            for (int i = 0; i < this.particle.length; i++) {
                this.particle[i] = this.particle[i].replace(refName, "");
                index++;
            }
            this.globalParticle = refName;
        }

        if (refDataCheck) {
            this.particle = null;
            this.globalParticleData = refData;
        }


    }

    public String getName() { return this.name; }

    public LoopedParticleEffect construct() {
        ArrayList prefabs = new ArrayList<ParticlePrefab>();
        int compressionMode = 0;

        if (this.globalParticle != null) {
            compressionMode++;
            if (this.globalParticleData != null) {
                compressionMode++;
            }
        }

        for (int index = 0; index < this.posx.length; index++) {
            ParticleEffect particle = ParticleTypes.BARRIER;
            Class c = net.minecraft.particle.ParticleTypes.class;

            String particleData;
            String particleBase;
            switch (compressionMode) {
                case 1:
                    particleBase = this.globalParticle;
                    particleData = this.particle[index];
                    break;
                case 2:
                    particleBase = this.globalParticle;
                    particleData = this.globalParticleData;
                    break;
                default:
                    particleData = this.particle[index];
                    if (particleData.indexOf(' ') != -1) {
                        particleBase = particleData.substring(0, particleData.indexOf(' '));
                        particleData = particleData.substring(particleBase.length()+1);
                    }
                    else {
                        particleBase = particleData;
                        particleData = "";
                    }
                    break;
            }


            switch(particleBase) {
                case "dust":
                    particle = new DustParticlePrefs(particleData).build();
                    break;

                case "item":
                    ParticleType<ItemStackParticleEffect> itemSpecial = null;
                    try {
                        itemSpecial = (ParticleType<ItemStackParticleEffect>) c.getField(particleBase.toUpperCase()).get(this);
                    } catch(Exception e) {
                        System.out.println("Unable to find type for particle effect " + particleBase + ". Defaulting to barrier..");
                    }

                    if (itemSpecial != null) {
                        particle = new ItemStackParticlePrefs(itemSpecial, particleData).build();
                    }
                    break;

                case "block":
                case "falling_dust":
                    ParticleType<BlockStateParticleEffect> blockSpecial = null;
                    try {
                        blockSpecial = (ParticleType<BlockStateParticleEffect>) c.getField(particleBase.toUpperCase()).get(this);
                    } catch(Exception e) {
                        System.out.println("Unable to find type for particle effect " + particleBase + ". Defaulting to barrier..");
                    }

                    if (blockSpecial != null) {
                        particle = new BlockParticlePrefs(blockSpecial, particleData).build();
                    }
                    break;

                default:
                    try {
                        particle = (ParticleEffect) c.getField(particleBase.toUpperCase()).get(this);
                    } catch(Exception e) {
                        System.out.println("Unable to find type for particle effect " + particleBase + ". Defaulting to barrier..");
                    }
                    break;
            }

            prefabs.add(new ParticlePrefab(particle,
                    new Vector3d(this.posx[index], this.posy[index], this.posz[index]),
                    new Vector3d(this.deltax[index], this.deltay[index], this.deltaz[index]),
                    this.speed[index],
                    this.count[index],
                    this.keyframe[index]));
        }

        return new LoopedParticleEffect(this.name,
                prefabs,
                this.delay,
                this.offset,
                this.reverse)
                .setCustomRenderProps(this.renderCount,
                        this.renderSteps);
    }


}
