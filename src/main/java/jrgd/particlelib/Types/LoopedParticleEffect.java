package jrgd.particlelib.Types;

import net.minecraft.particle.ParticleEffect;

import java.util.ArrayList;

import static jrgd.particlelib.Calculations.Range.FrameInRange;

public class LoopedParticleEffect {
    private String name;
    private ArrayList<ParticlePrefab> data;
    private int delay; // delay in ticks before next group of data frames are played, must be a minimum of 1
    private int activeDelay = 0;
    private int offset; // offset the animation start by a number of frames
    private boolean reverse; // play a given animation last frame to first
    private int index;
    private int keyframes = 0;

    // added bits for compressed mode
    // compressed mode gets kicked in automatically if the following:
    //  if a given particle is the same for all frames
    private ParticleEffect compressionParticle = null;

    // raw overrides for renderer
    // only use these if you understand what you are doing

    private int renderCount = -1;
    private int renderSteps = -1;

    public LoopedParticleEffect(String Name, ArrayList<ParticlePrefab> ParticlePrefabs, int Delay, int Offset, boolean Reverse) {
        this.name = Name.replaceAll("[^a-zA-Z0-9-_]","");
        this.data = ParticlePrefabs;
        this.delay = Delay;
        if (Offset > 0)
            this.offset = Offset;
        else
            this.offset = 0;
        this.reverse = Reverse;

        // code for preventing the renderer from locking up the server
        // make sure you have at least one keyframed particle in your effect

        boolean passedCheck = false;

        for (ParticlePrefab p : data) {
            if (p.isKeyframe()) {
                passedCheck = true;
                keyframes++;
            }
        }

        if (passedCheck) this.index = FrameInRange(this.getSize(), 1 + (this.offset * (this.data.size() / this.keyframes)));
        else this.data.clear();

    }

    public int getSize() {
        return this.data.size();
    }

    public int getCount() {
        return this.keyframes;
    }

    public ArrayList<ParticlePrefab> getData() {
        return this.data;
    }

    public int getDelay() {
        return this.delay;
    }

    public int getOffset() {
        return this.offset;
    }

    public String getName() {
        return this.name;
    }

    public int getIndex() {
        return this.index;
    }

    public int getRenderCount() {
        return this.renderCount;
    }

    public int getRenderSteps() {
        return this.renderSteps;
    }

    public boolean isReversed() {
        return this.reverse;
    }

    public boolean isActive() {
        if (this.activeDelay == 0) {
            this.activeDelay = this.delay - 1;
            return true;
        }
        else {
            this.activeDelay--;
            return false;
        }
    }

    public void decrementActive() {
        activeDelay = delay-1;
    }

    public LoopedParticleEffect setCustomRenderProps(int Count, int Steps) {
        this.renderCount = Count;
        this.renderSteps = Steps;
        return this;
    }

    public void overrideIndex(int NewIndex) {
        this.index = NewIndex;
    }

    public int getNextKeyframe() {
        ArrayList<Boolean> keyframes = new ArrayList<>();
        for (ParticlePrefab p : data) {
            keyframes.add(p.isKeyframe());
        }

        for (int i = 0; i < index+1; i++) {
            keyframes.add(keyframes.get(0));
            keyframes.remove(0);
        }

        int dist = keyframes.indexOf(true) + 1; //0+

        return FrameInRange(keyframes.size(), index+dist);
    }

    public int getPreviousKeyframe() {
        int tempIndex = index;
        boolean foundKeyed = false;
        while (!foundKeyed) {
            if (reverse)
                tempIndex = FrameInRange(this.getSize(), tempIndex++);
            else
                tempIndex = FrameInRange(this.getSize(), tempIndex--);
            foundKeyed = data.get(tempIndex).isKeyframe();
        }
        return tempIndex;
    }

    public ParticlePrefab nextFrame() {
        if (reverse) {
            index = FrameInRange(this.getSize(),index -1);
        }
        else {
            index = FrameInRange(this.getSize(),index +1);
        }
        return data.get(index - 1);
    }

    public ParticlePrefab previousFrame() {
        if (reverse) {
            index = FrameInRange(this.getSize(),index +1);
        }
        else {
            index = FrameInRange(this.getSize(),index -1);
        }
        return data.get(index - 1);
    }

}
