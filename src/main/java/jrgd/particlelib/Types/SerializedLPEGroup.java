package jrgd.particlelib.Types;

import java.io.Serializable;
import java.util.ArrayList;

import static jrgd.particlelib.LoopedParticleRegistry.GetEffect;

public class SerializedLPEGroup implements Serializable {
    private String[] group;
    private String name;

    public SerializedLPEGroup(LPEGroup Group) {
        this.name = Group.getName();
        this.group = new String[Group.getEffects().size()];
        int i = 0;
        for (LoopedParticleEffect p : Group.getEffects()) {
            group[i] = p.getName();
            i++;
        }
    }

    public LPEGroup construct() {
        ArrayList<LoopedParticleEffect> dump = new ArrayList<>();
        for (String e : group) {
            dump.add(GetEffect(e));
        }
        return new LPEGroup(this.name, dump);
    }

    public String getName() {
        return this.name;
    }
}
