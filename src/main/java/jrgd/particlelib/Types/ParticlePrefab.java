package jrgd.particlelib.Types;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleType;
import net.minecraft.server.world.ServerWorld;

public class ParticlePrefab {
    private ParticleEffect particle;
    private Vector3d position;
    private Vector3d option; // random offset range or velocity depending on LPE flag
    private double speed; // speed of particle effect according to minecraft
    private int count;
    private boolean keyframe; // used to indicate a set of prefabs to play in series
    // keyframe signifies a beginning point (a primary frame so to speak)

    public ParticlePrefab(ParticleEffect type, Vector3d pos, Vector3d Option, double Speed, int Count) {
        this.particle = type;
        this.position = pos;
        this.option = Option;
        this.speed = Speed;
        this.count = Count;
        this.keyframe = false;

    }

    public ParticlePrefab(ParticleEffect type, Vector3d pos, Vector3d Option, double Speed, int Count, boolean Keyframe) {
        this.particle = type;
        this.position = pos;
        this.option = Option;
        this.speed = Speed;
        this.count = Count;
        this.keyframe = Keyframe;

    }

    public ParticlePrefab(ParticleEffect type, Object data, Vector3d pos, Vector3d Option, double Speed, int Count) {
        this.particle = type;
        this.position = pos;
        this.option = Option;
        this.speed = Speed;
        this.count = Count;
        this.keyframe = false;

    }

    public ParticlePrefab(ParticleEffect type, Object data, Vector3d pos, Vector3d Option, double Speed, int Count, boolean Keyframe) {
        this.particle = type;
        this.position = pos;
        this.option = Option;
        this.speed = Speed;
        this.count = Count;
        this.keyframe = Keyframe;

    }

    public ParticleEffect getParticle() {
        return particle;
    }

    public Vector3d getPosition() {
        return position;
    }

    public Vector3d getOption() {
        return option;
    }

    public double getSpeed() {
        return speed;
    }

    public int getCount() {return count;}

    public boolean isKeyframe() {
        return keyframe;
    }

    public ParticlePrefab setParticleEffect(ParticleType type) {
        this.particle = (ParticleEffect) type;
        return this;
    }

    public ParticlePrefab setParticleEffect(ParticleEffect type) {
        this.particle = type;
        return this;
    }

    public ParticlePrefab setPosition(Vector3d Position) {
        this.position = Position;
        return this;
    }

    public ParticlePrefab setPosition(double x, double y, double z) {
        this.position = new Vector3d(x, y, z);
        return this;
    }

    public ParticlePrefab setOption(Vector3d Option) {
        this.option = Option;
        return this;
    }

    public ParticlePrefab setOption(double x, double y, double z) {
        this.option = new Vector3d(x, y, z);
        return this;
    }

    public ParticlePrefab setSpeed(double s) {
        this.speed = s;
        return this;
    }

    public ParticlePrefab setCount(int c) {
        this.count = c;
        return this;
    }

    public void spawnWorld(ServerWorld world, Vector3d Position) {
        world.spawnParticles(particle, Position.x + position.x, Position.y + position.y, Position.z + position.z, count, option.x, option.y, option.z, speed );
    }

    public void spawnPlayer(PlayerEntity player) {
        ServerWorld activeWorld = (ServerWorld) player.getEntityWorld();
        activeWorld.spawnParticles(particle, player.getX() + position.x, player.getY() + position.y, player.getZ() + position.z, count, option.x, option.y, option.z, speed );
    }

}
