package jrgd.particlelib.Types.ParticlePrefs;

import net.minecraft.particle.ParticleEffect;

public interface ParticlePrefs {
    public ParticleEffect build();
}
