package jrgd.particlelib.Types.ParticlePrefs;

import net.minecraft.particle.DustParticleEffect;

public class DustParticlePrefs implements ParticlePrefs{
    float r;
    float g;
    float b;
    float scale;

    public DustParticlePrefs(float Red, float Green, float Blue, float Scale) {
        this.r = Red;
        this.b = Blue;
        this.g = Green;
        this.scale = Scale;
    }

    public DustParticlePrefs(String data) {
        this.r = Float.parseFloat(data.substring(0,data.indexOf(' ')));
        data = data.substring(data.indexOf(' ') + 1);
        this.g = Float.parseFloat(data.substring(0,data.indexOf(' ')));
        data = data.substring(data.indexOf(' ') + 1);
        this.b = Float.parseFloat(data.substring(0,data.indexOf(' ')));
        data = data.substring(data.indexOf(' ') + 1);
        this.scale = Float.parseFloat(data);
    }

    public DustParticleEffect build() {
        return new DustParticleEffect(this.r, this.g, this.b, this.scale);
    }
}
