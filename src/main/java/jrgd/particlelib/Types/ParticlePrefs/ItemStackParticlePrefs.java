package jrgd.particlelib.Types.ParticlePrefs;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.StringTag;
import net.minecraft.particle.ItemStackParticleEffect;
import net.minecraft.particle.ParticleType;

public class ItemStackParticlePrefs implements ParticlePrefs{
    ItemStack i;
    ParticleType<ItemStackParticleEffect> base;

    public ItemStackParticlePrefs(ParticleType<ItemStackParticleEffect> base, ItemStack Item) {
        this.i = ItemHack(Item);
        this.base = base;
    }

    public ItemStackParticlePrefs(ParticleType<ItemStackParticleEffect> base, String data) {
        Class c = net.minecraft.item.Items.class;
        Item g = Items.STONE;

        try {
             g = (Item) c.getField(data.toUpperCase()).get(this);
        } catch(Exception e) {
            System.out.println("Unable to find type for item " + data + ". Defaulting to stone..");
        }

        this.i = ItemHack(new ItemStack(g));
        this.base = base;
    }

    public ItemStackParticleEffect build() {
        return new ItemStackParticleEffect(base, this.i);
    }

    public ItemStack getItemStack() {
        return this.i;
    }

    static ItemStack ItemHack(ItemStack Item) {
        Item.putSubTag("ISPE_DEF",StringTag.of(Item.getItem().toString()));
        return Item;
    }
}
