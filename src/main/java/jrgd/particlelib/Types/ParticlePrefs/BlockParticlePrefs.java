package jrgd.particlelib.Types.ParticlePrefs;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.particle.BlockStateParticleEffect;
import net.minecraft.particle.ParticleType;

public class BlockParticlePrefs implements ParticlePrefs {
    Block b;
    ParticleType<BlockStateParticleEffect> base;

    public BlockParticlePrefs(ParticleType<BlockStateParticleEffect> particle, Block block) {
        this.b = block;
        this.base = particle;
    }

    public BlockParticlePrefs(ParticleType<BlockStateParticleEffect> particle, String data) {
        Class c = net.minecraft.block.Blocks.class;
        Block g = Blocks.STONE;

        try {
            g = (Block) c.getField(data.toUpperCase()).get(this);
        } catch(Exception e) {
            System.out.println("Unable to find type for block " + data + ". Defaulting to stone..");
        }

        this.b = g;
        this.base = particle;
    }

    public BlockStateParticleEffect build() {
        return new BlockStateParticleEffect(base, this.b.getDefaultState());
    }
}
