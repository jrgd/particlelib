package jrgd.particlelib;

import jrgd.particlelib.Types.*;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.dimension.DimensionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static jrgd.particlelib.io.*;

public class LoopedParticleRegistry {
    private static Map<String, LoopedParticleEffect> renderedEffects = new HashMap<String, LoopedParticleEffect>();
    private static Map<String, LPEGroup> effectGroups = new HashMap<String, LPEGroup>();
    private static ArrayList<LoopedParticleContainer> activeParticles = new ArrayList<>();

    private static ArrayList<LoopedParticleContainer> markedForRemoval = new ArrayList<>();

    //private static ArrayList<String> SlowRenderQueue;

    public static void AddContainer(LoopedParticleContainer container) {
        activeParticles.add(container);
    }

    public static LoopedParticleEffect AddEffect(LoopedParticleEffect effect) {
        renderedEffects.put(effect.getName(), effect);
        return renderedEffects.get(effect.getName());
    }

    public static LPEGroup AddGroup(LPEGroup group) {
        effectGroups.put(group.getName(), group);
        return effectGroups.get(group.getName());
    }



    public static boolean SaveEffect(LoopedParticleEffect effect) {
        return serializeEffectOut(new SerializedLPE(effect));
    }

    public static boolean SaveGroup(LPEGroup group) {
        return serializeGroupOut(new SerializedLPEGroup(group));
    }



    public static LoopedParticleEffect GetEffect(String name) {
        return renderedEffects.get(name);
    }

    public static LPEGroup GetGroup(String name) {
        return effectGroups.get(name);
    }



    public static ArrayList<String> ListEffects() {
        ArrayList out = new ArrayList<String>();
        renderedEffects.forEach((name, effect) -> out.add(name));
        return out;
    }

    public static ArrayList<String> ListGroups() {
        ArrayList out = new ArrayList<String>();
        effectGroups.forEach((name, group) -> out.add(name));
        return out;
    }



    public static void RemoveContainer(LoopedParticleEffect name) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getLoopedParticle().equals(name)) markedForRemoval.add(c);
        }
    }

    public static void RemoveContainer(ServerPlayerEntity player) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getPlayer() == player) markedForRemoval.add(c);
        }
    }

    public static void RemoveContainer(ServerWorld world) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getWorld() != null) {
                if (c.getWorld() == world) markedForRemoval.add(c);
            }
            else {
                ServerWorld playerContext = (ServerWorld) c.getPlayer().getEntityWorld();
                if (playerContext == world) markedForRemoval.add(c);
            }

        }
    }

    public static void RemoveContainer(DimensionType world) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getWorld() != null) {
                if (c.getWorld().getDimension().getType() == world) markedForRemoval.add(c);
            }
            else {
                ServerWorld playerContext = (ServerWorld) c.getPlayer().getEntityWorld();
                if (playerContext.getDimension().getType() == world) markedForRemoval.add(c);
            }
        }
    }

    public static void RemoveContainer(ServerPlayerEntity player, LoopedParticleEffect name) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getPlayer() == player && c.getLoopedParticle().equals(name)) markedForRemoval.add(c);
        }
    }

    public static void RemoveContainer(ServerWorld world, LoopedParticleEffect name) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getWorld() != null) {
                if (c.getWorld() == world && c.getLoopedParticle().equals(name)) markedForRemoval.add(c);
            }
            else {
                ServerWorld playerContext = (ServerWorld) c.getPlayer().getEntityWorld();
                if (playerContext == world && c.getLoopedParticle().equals(name)) markedForRemoval.add(c);
            }
        }
    }

    public static void RemoveContainer(DimensionType world, LoopedParticleEffect name) {
        for (LoopedParticleContainer c : activeParticles) {
            if (c.getWorld() != null) {
                if (c.getWorld().getDimension().getType() == world && c.getLoopedParticle().equals(name)) markedForRemoval.add(c);
            }
            else {
                ServerWorld playerContext = (ServerWorld) c.getPlayer().getEntityWorld();
                if (playerContext.getDimension().getType() == world && c.getLoopedParticle().equals(name)) markedForRemoval.add(c);
            }
        }
    }

    public static boolean RemoveEffect(String name) {
        if (renderedEffects.get(name) != null) {
            RemoveContainer(GetEffect(name)); // remove instances before yeeting from thy existence
            renderedEffects.remove(name);
            return true;
        }
        else return false;
    }

    public static boolean RemoveGroup(String name) {
        if (effectGroups.get(name) != null) {
            effectGroups.remove(name);
            return true;
        }
        else return false;
    }

    public static boolean DeleteEffect(String name) {
        boolean check = RemoveEffect(name);
        if (check) {
            check = DeleteFile(name);
        }
        return check;
    }

    public static boolean deleteGroup(String name) {
        boolean check = RemoveGroup(name);
        if (check) {
            check = DeleteFile("!" + name);
        }
        return check;
    }

    // Renderer for active particle effects
    // The LoopedParticleEffect type expects the renderer to be like this
    // Changes may break expected functionality

    public static void RenderActive() {

        for (LoopedParticleContainer c : activeParticles) {
            // run the isActive routine | output based on a LPE's delayTicks
            // if true, run the renderFrame method, otherwise skip
            if (c.getLoopedParticle().isActive()) Renderer.RenderFrame(c);

            // Mark effects for removal if a given player has disconnected
            if (c.isEntityReference()) {
                ServerPlayerEntity player = (ServerPlayerEntity) c.getPlayer();
                // method 14239 states whether or not a player has disconnected, is dead apparently
                // hopefully this gets cleared up in the future, as dead makes only some sense
                if (player.method_14239()) markedForRemoval.add(c);
            }
        }

        // Remove effect if it was bound to a now-disconnected player
        for (LoopedParticleContainer c : markedForRemoval) {
            activeParticles.remove(c);
        }
        markedForRemoval.clear();
    }


}
