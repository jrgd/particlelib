package jrgd.particlelib;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.server.ServerTickCallback;

import static jrgd.particlelib.LoopedParticleRegistry.RenderActive;
import static jrgd.particlelib.io.*;

public class Main implements ModInitializer {

	@Override
	public void onInitialize() {
		// if saved effects directory doesn't exist, make it
		directoryPath.mkdir();

		// load in saved effects

		int effectSuccess = 0;
		int groupSuccess = 0;
		// load effects first
		// groups will grab onto raw effects rather name keyvalues
		// making this order necessary
		for (String file : getDataFiles()) {
			if (LoadFile(file, false)) effectSuccess++;
		}
		// load groups after effects
		for (String file : getDataFiles()) {
			if (LoadFile(file, true)) groupSuccess++;
		}

		System.out.println("ParticleLib successfully loaded " + effectSuccess + " LPEs.");
		System.out.println("ParticleLib successfully loaded " + groupSuccess + " LPEGroups.");

		ServerTickCallback.EVENT.register((server) -> RenderActive());
	}

}
