package jrgd.particlelib;

import jrgd.particlelib.Types.LoopedParticleContainer;
import jrgd.particlelib.Types.LoopedParticleEffect;
import jrgd.particlelib.Types.ParticlePrefab;

public class Renderer {
    static void RenderFrame(LoopedParticleContainer c) {
        LoopedParticleEffect part = c.getLoopedParticle();
        ParticlePrefab activeFrame = part.nextFrame();

        // Parts for explicit render definition
        // this stuff does not affect normal render operation
        // please ignore unless knowing how to explicitly utilize this

        boolean explicitDefinition = false;
        int explicitRenderCount = 0;
        int explicitRenderSteps = 0;

        if (part.getRenderCount() > -1 || part.getRenderSteps() > -1) {
            explicitDefinition = true;
            if (part.getRenderCount() > -1) explicitRenderCount = part.getRenderCount();
            if (part.getRenderSteps() > -1) explicitRenderSteps = part.getRenderSteps();

        }

        // get general frame and indexing information
        if (!explicitDefinition) {
            // enter interpreted renderer
            // this follows the rules that builders like Orbiter set for a given LPE
            // for an example of a compliant builder, check out /jrgd/orbiter on gitlab

            int frameIndex = part.getIndex();
            int targetFrame = part.getNextKeyframe();
            int count; // frames to play now

            // Calculate the amount of frames til next keyframe

            if (targetFrame < frameIndex) {
                int size = part.getSize();
                count = (part.getSize() - frameIndex) + targetFrame;
            }
            else {
                count = targetFrame - frameIndex;
            }

            // reset if a given frame
            if (count == -1)
                count = 1;
            for (; count > 0; count--) {

                if (c.isEntityReference()) activeFrame.spawnPlayer(c.getPlayer());
                else activeFrame.spawnWorld(c.getWorld(), c.getWorldCoordinates());

                if (count > 1) activeFrame = part.nextFrame();
            }
        }
        else {
            // enter explicit renderer
            // this thing does not follow the default rules that Orbiter and other LPE builds set
            // this will always increment x amount of frames, and scale back based on steps

            // For instance, having frames [0,1,2,3,4,5,6,7,8,9]
            // having RenderCount to 4 and RenderSteps to 1
            // you will render things like [0,1,2,3], [1,2,3,4], [2,3,4,5], etc.
            // this varies greatly from the interpreted renderer, which reads from keyframes
            // this renderer, while far more primitive, is useful for certain types of effects

            int count = explicitRenderCount;

            for (; count > 0; count--) {

                if (c.isEntityReference()) activeFrame.spawnPlayer(c.getPlayer());
                else activeFrame.spawnWorld(c.getWorld(), c.getWorldCoordinates());

                if (count > 1) activeFrame = part.nextFrame();
            }
            for (int back = (explicitRenderCount - explicitRenderSteps); back > 0; back--) {
                part.previousFrame();
            }
        }
    }
}
