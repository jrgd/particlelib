package jrgd.particlelib.Calculations;

public class Range {
    public static int FrameInRange(int range, int frame) {
        // frames technically start at one, for special purposes
        // this gives 0 special properties to smoothly start animations
        int currentFrame;
        if (frame > range)
            currentFrame = frame - ((frame / range) * range);
        else if (frame < 1)
            currentFrame = range - (frame - ((frame * -1) / range) * range);
        else
            currentFrame = frame;
        return currentFrame;
    }

    public static double NumberInRange(double percentage, double p1, double p2) {
        return p1 + ((p2 - p1) * percentage);
    }
}
