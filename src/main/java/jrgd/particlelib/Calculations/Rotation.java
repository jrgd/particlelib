package jrgd.particlelib.Calculations;

import jrgd.particlelib.Types.Vector3d;

public class Rotation {
    public static Vector3d ToRadians(Vector3d rotation) {
        rotation.x = Math.toRadians(rotation.x);
        rotation.y = Math.toRadians(rotation.y);
        rotation.z = Math.toRadians(rotation.z);
        return rotation;
    }

    public static Vector3d ToDegrees(Vector3d rotation) {
        rotation.x = Math.toDegrees(rotation.x);
        rotation.y = Math.toDegrees(rotation.y);
        rotation.z = Math.toDegrees(rotation.z);
        return rotation;
    }

    public static Vector3d CircleGenerator(int frames, int step) {
        Vector3d position = new Vector3d(0,0,0);
        position.x = Math.cos(Math.toRadians(step * 360 * (1/(double)frames)));
        position.z = Math.sin(Math.toRadians(step * 360 * (1/(double)frames)));
        return position;
    }

    public static Vector3d SineOnY(int frames, int step, double scale) {
        Vector3d position = new Vector3d(0,0,0);
        position.y = scale * Math.sin(Math.toRadians(step * 360 * (1/(double)frames)));
        return position;
    }

    // The following five methods are an implementation for rotation around a central point
    // https://en.wikipedia.org/wiki/Rotation_matrix#General_rotations

    private static double[] MatrixRotation_X(double input) {
        double[] rotation = new double[]{
                1,
                0,
                0,
                0,
                Math.cos(input),
                Math.sin(input),
                0,
                Math.sin(input) * -1,
                Math.cos(input)
        };
        return rotation;
    }

    private static double[] MatrixRotation_Y(double input) {
        double[] rotation = new double[]{
                Math.cos(input),
                0,
                Math.sin(input) * -1,
                0,
                1,
                0,
                Math.sin(input),
                0,
                Math.cos(input)
        };
        return rotation;
    }

    private static double[] MatrixRotation_Z(double input) {
        double[] rotation = new double[]{
                Math.cos(input),
                Math.sin(input),
                0,
                Math.sin(input) * -1,
                Math.cos(input),
                0,
                0,
                0,
                1
        };
        return rotation;
    }

    private static Vector3d ComputeRotation(double[] matrix, Vector3d pos) {
        pos = new Vector3d(
                (matrix[0] * pos.x) + (matrix[3] * pos.y) + (matrix[6] * pos.z),
                (matrix[1] * pos.x) + (matrix[4] * pos.y) + (matrix[7] * pos.z),
                (matrix[2] * pos.x) + (matrix[5] * pos.y) + (matrix[8] * pos.z));
        return pos;

    }

    public static Vector3d ApplyRotation(Vector3d position, Vector3d rotation) {
        position = ComputeRotation(MatrixRotation_X(rotation.x), position);
        position = ComputeRotation(MatrixRotation_Y(rotation.y), position);
        position = ComputeRotation(MatrixRotation_Z(rotation.z), position);

        return position;
    }
}
